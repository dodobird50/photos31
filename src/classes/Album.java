package classes;

import java.io.Serializable;
import java.util.ArrayList;

/**
 * The Album class represents an album
 * 
 * @author Eric Zhang
 * @author Stanley Cai
 *
 */
public class Album implements Serializable {

	private static final long serialVersionUID = 1L;
	/**
	 * Name of the Album
	 */
	private String name;
	/**
	 * ArrayList of photos that contain all the photos of the Album
	 */
	private final ArrayList<Photo> photos;

	/**
	 * Constructor for an instance of an Album
	 * 
	 * @param name - name of the Album
	 */
	public Album( String name ) {
		this.name = name;
		photos = new ArrayList<>();
	}

	/**
	 * Constructor for an instance of an Album
	 * 
	 * @param name   - name of the Album
	 * @param photos - a list of Photos in the Album
	 */
	public Album( String name, ArrayList<Photo> photos ) {
		this.name = name;
		this.photos = photos;
	}

	/**
	 * Retrieve the name of the Album
	 * 
	 * @return name of the Album
	 */
	public String getName() {
		return name;
	}
	
	/**
	 * Change the name of the Album
	 * 
	 * @param name - the new name of the Album
	 */
	public void setName( String name ) {
		this.name = name;
	}

	/**
	 * Remove a photo from the Album
	 * 
	 * @param photo - specific photo to be removed
	 * @return true if photo was successfully removed; false otherwise
	 */
	public boolean deletePhoto( Photo photo ) {
		return photos.remove( photo );
	}

	/**
	 * Add a photo to the Album
	 * 
	 * @param photo - specific photo to be added
	 * @return true if photo was successfully added; false otherwise
	 */
	public boolean addPhoto( Photo photo ) {
		for ( Photo existingPhoto : photos ) {
			if ( existingPhoto.equals( photo ) ) {
				return false;
			}
		}
		photos.add( photo );
		return true;
	}

	/**
	 * Retrieve a list of all the photos in the Album
	 * 
	 * @return arraylist of photos in the Album
	 */
	public ArrayList<Photo> getPhotos() {
		return photos;
	}

	/**
	 * Check if another Album is equal to this Album (i.e. has the same name)
	 * 
	 * @param other - other Album's name
	 * @return true if both Albums are equal; false otherwise
	 */
	public boolean equals( Album other ) {
		return name.equalsIgnoreCase( other.name );
	}

	@Override
	public String toString() {
		return name;
	}
}
