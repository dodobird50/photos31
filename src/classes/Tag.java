package classes;

import java.io.Serializable;
/**
 * The Tag class represents a tag that a Photo can contain
 * 
 * @author Eric Zhang
 * @author Stanley Cai
 *
 */
public class Tag implements Serializable, Comparable<Tag> {

	private static final long serialVersionUID = 1L;
	/**
	 * Key of the Tag
	 */
	private final String key;
	/**
	 * Value of the Tag
	 */
	private final String value;
	
	/**
	 * Constructor for an instance of a Tag
	 * @param key - key of the Tag
	 * @param value - value of the Tag
	 */
	public Tag( String key, String value ) {
		this.key = key;
		this.value = value;
	}

	/**
	 * Retrieve the key of the Photo
	 * @return key of the Photo
	 */
	public String getKey() {
		return key;
	}

	/**
	 * Retrieve the value of the Photo
	 * @return value of the Photo
	 */
	public String getValue() {
		return value;
	}
	
	/**
	 * Check if two Tags are equal (i.e. have the same key and value)
	 * @param other - another Tag to compare to this Tag
	 * @return true if both Tags are equal; false otherwise
	 */
	public boolean equals( Tag other ) {
		return key.equalsIgnoreCase( other.key ) && value.equalsIgnoreCase( other.value );
	}
	
	/**
	 * Compare another Tag to this Tag based on key, then value
	 * @param other - another Tag to compare to this Tag
	 * @return equality relationship between this Tag and the other Tag
	 */
	public int compareTo( Tag other ) {
		int c = key.compareToIgnoreCase( other.key );
		if ( c == 0 ) {
			return value.compareToIgnoreCase( other.value );
		}
		else {
			return c;
		}
	}
	
	public String toString() {
		return "\"" + key + "\":" + "\"" + value + "\"";
	}
}
