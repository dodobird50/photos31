package classes;

import java.util.ArrayList;
import java.util.HashSet;

import javafx.collections.FXCollections;
import javafx.collections.ObservableList;

/**
 * The User class represents a user
 * 
 * @author Eric Zhang
 * @author Stanley Cai
 *
 */
public class User {
	/**
	 * Username of the User
	 */
	private String username;
	/**
	 * ObservableList of the user's albums
	 */
	private ObservableList<Album> albums;

	/**
	 * Constructor for an instance of a User
	 * 
	 * @param username - username of the User
	 */
	public User( String username ) {
		this.username = username;
		albums = FXCollections.observableArrayList();
	}

	/**
	 * Constructor for an instance of a User
	 * 
	 * @param username - username of the User
	 * @param albums   - an ArrayList of Albums
	 */
	public User( String username, ArrayList<Album> albums ) {
		this.username = username;
		this.albums = FXCollections.observableArrayList( albums );
	}

	/**
	 * Constructor for an instance of an User
	 * 
	 * @param username - username of the User
	 * @param albums   - an ObservableList of Albums
	 */
	public User( String username, ObservableList<Album> albums ) {
		this.username = username;
		this.albums = albums;
	}

	/**
	 * Add an Album
	 * 
	 * @param album - the Album to be added
	 * @return true if successfully added the Album; false otherwise
	 */
	public boolean addAlbum( Album album ) {
		for ( Album existingAlbum : albums ) {
			if ( existingAlbum.equals( album ) ) {
				return false;
			}
		}

		albums.add( album );
		return true;
	}

	/**
	 * Determine if an existing Album can be renamed
	 * 
	 * @param album        - Album to be renamed
	 * @param newAlbumName - new name for the Album
	 * @return true if the Album can be renamed to the new name; false otherwise
	 */
	public boolean canRenameAlbum( Album album, String newAlbumName ) {
		if ( albums.indexOf( album ) == -1 ) {
			return false;
		}

		for ( Album existingAlbum : albums ) {
			if ( existingAlbum == album && existingAlbum.getName().equals( newAlbumName ) ) {
				return false;
			}
			else if ( existingAlbum != album && existingAlbum.getName().equalsIgnoreCase( newAlbumName ) ) {
				return false;
			}
		}

		return true;
	}

	/**
	 * Search for a specific Album by name
	 * 
	 * @param albumName - name of the Album to be searched for
	 * @return an Album with a matching name
	 */
	public Album lookupAlbum( String albumName ) {
		for ( Album album : albums ) {
			if ( album.getName().equals( albumName ) ) {
				return album;
			}
		}

		return null;
	}

	/**
	 * Retrieve all photos under the User
	 * 
	 * @return ArrayList of photos that are under the User
	 */
	public ArrayList<Photo> getAllPhotos() {
		HashSet<Photo> temp = new HashSet<>();

		for ( Album album : albums ) {
			for ( Photo photo : album.getPhotos() ) {
				temp.add( photo );
			}
		}

		ArrayList<Photo> allPhotos = new ArrayList<>();
		for ( Photo photo : temp ) {
			allPhotos.add( photo );
		}

		return allPhotos;
	}

	/**
	 * Delete an Album
	 * 
	 * @param album - Album object to be deleted
	 * @return true if Album was successfully deleted; false otherwise
	 */
	public boolean deleteAlbum( Album album ) {
		return albums.remove( album );
	}

	/**
	 * Retrieve username of the User
	 * 
	 * @return username of the User
	 */
	public String getUsername() {
		return username;
	}

	/**
	 * Retrieve an ObservableList of Albums of the User
	 * 
	 * @return an ObservableList of Albums of the User
	 */
	public ObservableList<Album> getAlbums() {
		return albums;
	}

	/**
	 * Retrieve the ArrayList of Albums of the User
	 * 
	 * @return an ArrayList of Albums of the User
	 */
	public ArrayList<Album> getAlbumsArrayList() {
		ArrayList<Album> temp = new ArrayList<>();
		for ( Album album : albums ) {
			temp.add( album );
		}

		return temp;
	}

	@Override
	public String toString() {
		return username;
	}
}
