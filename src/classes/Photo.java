package classes;

import java.io.File;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Calendar;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
/**
 * The Photo class represents a photo
 * 
 * @author Eric Zhang
 * @author Stanley Cai
 *
 */
public class Photo implements Serializable {
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	/**
	 * Specific file of the photo in the disk; is of the following formats: BMP, GIF, JPEG, PNG
	 */
	private File photoFile;
	/**
	 * Caption of the Photo
	 */
	private String caption;
	/**
	 * ArrayList of tags associated with the Photo
	 */
	private ArrayList<Tag> tags;

	/**
	 * Constructor for an instance of a Photo
	 * @param photoFile - specific file of the Photo in the disk
	 */
	public Photo( File photoFile ) {
		this.photoFile = photoFile;
		caption = "";
		tags = new ArrayList<>();
	}
	
	/**
	 * Retrieve an ObservableList of Tags
	 * @return an ObservableList of Tags
	 */
	public ObservableList<Tag> getTagsObservableList() {
		ObservableList<Tag> temp = FXCollections.observableArrayList();
		for ( Tag tag : tags ) {
			temp.add( tag );
		}

		return temp;
	}

	/**
	 * Sync the Photo's Arraylist of Tags to an external ObservableList of Tags
	 * @param tags - an ObservableList of tags
	 */
	public void setTags( ObservableList<Tag> tags ) {
		this.tags.clear();
		for ( Tag tag : tags ) {
			this.tags.add( tag );
		}
	}

	/**
	 * Check if the Photo contains the given Tag
	 * @param tag - Tag to be checked
	 * @return true if the photo contains the given Tag; false otherwise
	 */
	public boolean lookupTag( Tag tag ) {
		for ( Tag existingTag : tags ) {
			if ( existingTag.equals( tag ) ) {
				return true;
			}
		}
		
		return false;
	}
	
	/**
	 * Retrieve the caption of the Photo
	 * @return caption of the Photo
	 */
	public String getCaption() {
		return caption;
	}
	
	/**
	 * Set the caption of the Photo
	 * @param caption - updated caption of the Photo
	 */
	public void setCaption( String caption ) {
		this.caption = caption;
	}
	
	/**
	 * Retrieve the photo file associated with the Photo
	 * @return the photo file associated with the Photo
	 */
	public File getPhotoFile() {
		return photoFile;
	}

	/**
	 * Check if two photos are equal (i.e. have the same photo file)
	 * @param other - another Photo to compare to this one
	 * @return true if both Photos are equal; false otherwise
	 */
	public boolean equals( Photo other ) {
		return photoFile.getAbsolutePath().equalsIgnoreCase( other.photoFile.getAbsolutePath() );
	}

	/**
	 * Converts the modified time of the photo file into string form 
	 * @return modified time of the photo file in string form
	 */
	public String photoDateStringForm() {
		Calendar cal = Calendar.getInstance();
		cal.setTimeInMillis( photoFile.lastModified() );

		int year = cal.get( Calendar.YEAR );
		int month = cal.get( Calendar.MONTH ) + 1;
		int day = cal.get( Calendar.DAY_OF_MONTH );
		int hour = cal.get( Calendar.HOUR_OF_DAY );
		int minute = cal.get( Calendar.MINUTE );
		int second = cal.get( Calendar.SECOND );

		String date = month + "/" + day + "/" + year + ", " + hour + ":";

		if ( minute < 10 ) {
			date += "0";
		}
		date += minute + ":";

		if ( second < 10 ) {
			date += "0";
		}
		date += second;

		return date;
	}
	
	/**
	 * Retrieve the last modified time of the photo file
	 * @return last modified time of the photo file
	 */
	public long getPhotoTime() {
		return photoFile.lastModified();
	}
}
