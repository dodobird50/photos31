package states;

import photos31.Photos;
/**
 * Framework for all states to inherit
 * 
 * @author Eric Zhang
 * @author Stanley Cai
 *
 */
public abstract class State {
	/**
	 * Main photos application
	 */
	private final Photos application;
	
	/**
	 * Constructor for a state
	 * @param application - reference to the main photos application
	 */
	public State( Photos application ) {
		this.application = application;
	}
	
	/**
	 * Accept a signal from the source and relay control to the appropriate method.
	 * @param source - the specific source the signal is being received from
	 */
	public abstract void handleSignal( String source );
	
	/**
	 * Retrieve the main photos application
	 * @return photos application
	 */
	public Photos getApplication() {
		return application;
	}
}
