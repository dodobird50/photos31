package states;

import photos31.Photos;
/**
 * The state of being in the login screen
 * 
 * @author Eric Zhang
 * @author Stanley Cai
 *
 */
public class LoginScreenState extends State {

	/**
	 * Constructor for the login screen
	 * @param application - reference to the main photos application
	 */
	public LoginScreenState( Photos application ) {
		super( application );
	}

	/**
	 * Accept a signal from the source and relay control to the appropriate method.
	 */
	@Override
	public void handleSignal( String source ) {
		if ( source.equals( "loginButton" ) ) {
			getApplication().login();
		}
	}

}
