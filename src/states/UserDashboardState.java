package states;

import classes.User;
import photos31.Photos;
/**
 * The state of being in the user dashboard
 * 
 * @author Eric Zhang
 * @author Stanley Cai
 *
 */
public class UserDashboardState extends State {
	/**
	 * User accessing the state
	 */
	private User user;
	
	/**
	 * Constructor for the user dashboard
	 * @param application - reference to the main photos application
	 * @param user - user accessing the state
	 */
	public UserDashboardState( Photos application, User user ) {
		super( application );
		this.user = user;
	}
	
	/**
	 * Accept a signal from the source and relay control to the appropriate method.
	 */
	@Override
	public void handleSignal( String source ) {
		if ( source.equals( "searchPhotosButton" ) ) {
			getApplication().searchPhotos();
		}
		else if ( source.equals( "openAlbumButton" ) ) {
			getApplication().openSelectedAlbum();
		}
		else if ( source.equals( "createNewAlbumButton" ) ) {
			getApplication().createNewAlbum();
		}
		else if ( source.equals( "renameAlbumButton" ) ) {
			getApplication().renameSelectedAlbum();
		}
		else if ( source.equals( "deleteAlbumButton" ) ) {
			getApplication().deleteSelectedAlbum();
		}
		else if ( source.equals( "saveAndLogoutButton" ) ) {
			getApplication().logoutUser();
		}
	}
	
	/**
	 * Retrieve the user accessing the state
	 * @return user that is currently accessing the state
	 */
	public User getUser() {
		return user;
	}
}
