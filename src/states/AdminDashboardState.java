package states;

import classes.User;
import javafx.collections.ObservableList;
import photos31.Photos;
/**
 * The state of being in the admin dashboard
 * 
 * @author Eric Zhang
 * @author Stanley Cai
 *
 */
public class AdminDashboardState extends State {
	private ObservableList<User> userListForAdmin;
	
	/**
	 * Constructor for the admin dashboard
	 * @param application - reference to the main photos application
	 * @param userListForAdmin - observablelist of current admins
	 */
	public AdminDashboardState( Photos application, ObservableList<User> userListForAdmin ) {
		super( application );
		this.userListForAdmin = userListForAdmin;
	}
	
	/**
	 * Accept a signal from the source and relay control to the appropriate method.
	 */
	@Override
	public void handleSignal( String source ) {
		if ( source.equals( "createNewUserButton" ) ) {
			getApplication().createNewUser();
		}
		else if ( source.equals( "deleteUserButton" ) ) {
			getApplication().deleteSelectedUser();
		}
		else if ( source.equals( "saveAndLogoutButton" ) ) {
			getApplication().logoutAdmin();
		}
	}
	
	/**
	 * Retrieve the list of users for the admin
	 * @return observable list of all users
	 */
	public ObservableList<User> getUserListForAdmin() {
		return userListForAdmin;
	}
}
