package states;

import java.util.ArrayList;

import classes.Photo;
import classes.User;
import photos31.Photos;
/**
 * The state of being in the search results display
 * 
 * @author Eric Zhang
 * @author Stanley Cai
 *
 */
public class SearchResultsDisplayState extends State {
	/**
	 * User accessing the state
	 */
	private User user;
	/**
	 * ArrayList containing the relevant photos according to the search input
	 */
	private ArrayList<Photo> photos;
	/**
	 * Pointer to the current photo index
	 */
	private int photosIndex;
	/**
	 * Search input for finding relevant photos related to the input
	 */
	private String searchInput;

	/**
	 * Constructor for the search results display
	 * @param application - reference to the main photos application
	 * @param user - user accessing the state
	 * @param searchInput - search input for finding relevant photos
	 * @param photos - arraylist of relevant photos according to the search input
	 */
	public SearchResultsDisplayState( Photos application, User user, String searchInput, ArrayList<Photo> photos ) {
		super( application );
		this.user = user;
		this.photos = photos;
		this.searchInput = searchInput;
		if ( photos.size() == 0 ) {
			this.photosIndex = -1;
		}
		else {
			this.photosIndex = 0;
		}
	}

	/**
	 * Accept a signal from the source and relay control to the appropriate method.
	 */
	@Override
	public void handleSignal( String source ) {
		if ( source.equals( "goToNextPhotoButton" ) ) {
			getApplication().goToNextPhoto();
		}
		else if ( source.equals( "goToPreviousPhotoButton" ) ) {
			getApplication().goToPreviousPhoto();
		}
		else if ( source.equals( "createNewAlbumButton" ) ) {
			getApplication().createNewAlbumWithSearchResults();
		}
		else if ( source.equals( "backButton" ) ) {
			getApplication().returnToUserDashboard();
		}
	}

	/**
	 * Check if the currently pointed photo in the search results has a next photo
	 * @return true if there is a next photo; false otherwise
	 */
	public boolean hasNextPhoto() {
		return photosIndex + 1 < photos.size();
	}

	/**
	 * Increment the photo pointer to point to the next photo in the search results
	 */
	public void nextPhoto() {
		if ( photosIndex + 1 < photos.size() ) {
			photosIndex++;
		}
	}

	/**
	 * Check if the currently pointed photo in the search results has a previous photo
	 * @return true if there is a previous photo; false otherwise
	 */
	public boolean hasPreviousPhoto() {
		return photosIndex > 0;
	}

	/**
	 * Decrement the photo pointer to point to the previous photo in the search results
	 */
	public void previousPhoto() {
		if ( photosIndex > 0 ) {
			photosIndex--;
		}
	}
	
	/**
	 * Retrieve the current photo pointed in the search results
	 * @return photo specifically pointed to in the search results
	 */
	public Photo getCurrentPhoto() {
		if ( photosIndex == -1 ) {
			return null;
		}
		else {
			return photos.get( photosIndex );
		}
	}
	
	/**
	 * Retrieve the user currently accessing the state
	 * @return user currently accessing the state
	 */
	public User getUser() {
		return user;
	}
	
	/**
	 * Retrieve the entire arraylist of relevant photos according to the specific search input
	 * @return arraylist of relevant photos
	 */
	public ArrayList<Photo> getPhotos() {
		return photos;
	}
	
	/**
	 * Retrieve the search input for finding specific photos
	 * @return search input
	 */
	public String getSearchInput() {
		return searchInput;
	}
}
