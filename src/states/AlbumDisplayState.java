package states;

import classes.Album;
import classes.Photo;
import classes.User;
import photos31.Photos;
/**
 * The state of being in the album display
 * 
 * @author Eric Zhang
 * @author Stanley Cai
 *
 */
public class AlbumDisplayState extends State {
	/**
	 * User accessing the state
	 */
	private User user;
	/**
	 * Album being displayed
	 */
	private Album album;
	/**
	 * Pointer to photos in the album
	 */
	private int albumIndex;

	/**
	 * Constructor for the album display
	 * @param application - reference to the main photos application
	 * @param user - user accessing the album display
	 * @param album - album being displayed
	 */
	public AlbumDisplayState( Photos application, User user, Album album ) {
		super( application );
		this.user = user;
		this.album = album;
		if ( album.getPhotos().size() == 0 ) {
			this.albumIndex = -1;
		}
		else {
			this.albumIndex = 0;
		}
	}

	/**
	 * Accept a signal from the source and relay control to the appropriate method.
	 */
	public void handleSignal( String source ) {
		if ( source.equals( "goToNextPhotoButton" ) ) {
			getApplication().goToNextPhoto();
		}
		else if ( source.equals( "goToPreviousPhotoButton" ) ) {
			getApplication().goToPreviousPhoto();
		}
		else if ( source.equals( "deletePhotoButton" ) ) {
			getApplication().deleteCurrentPhoto();
		}
		else if ( source.equals( "addPhotosButton" ) ) {
			getApplication().selectPhotoToAddToAlbum();
		}
		else if ( source.equals( "addTagButton" ) ) {
			getApplication().addTagToPhoto();
		}
		else if ( source.equals( "deleteTagButton" ) ) {
			getApplication().deleteSelectedTag();
		}
		else if ( source.equals( "updateCaptionButton" ) ) {
			getApplication().updatePhotoCaption();
		}
		else if ( source.equals( "movePhotoToAlbumButton" ) ) {
			getApplication().movePhoto();
		}
		else if ( source.equals( "copyPhotoToAlbumButton" ) ) {
			getApplication().copyPhoto();
		}
		else if ( source.equals( "backButton" ) ) {
			getApplication().returnToUserDashboard();
		}
	}

	/**
	 * Retrieve the user accessing the album display state
	 * @return user accessing the state
	 */
	public User getUser() {
		return user;
	}

	/**
	 * Retrieve album being displayed in the album display state
	 * @return album being displayed in the state
	 */
	public Album getAlbum() {
		return album;
	}

	/**
	 * Check if album contains a photo after the currently pointed one
	 * @return true if there is a next photo; false otherwise
	 */
	public boolean hasNextPhoto() {
		return albumIndex + 1 < album.getPhotos().size();
	}

	/**
	 * Increment the album pointer to point to the next photo
	 */
	public void nextPhoto() {
		if ( albumIndex + 1 < album.getPhotos().size() ) {
			albumIndex++;
		}
	}

	/**
	 * Check if album contains a photo before the currently pointed one
	 * @return true if there is a previous photo; false otherwise
	 */
	public boolean hasPreviousPhoto() {
		return albumIndex > 0;
	}

	/**
	 * Decrement the album pointer to point to the previous photo
	 */
	public void previousPhoto() {
		if ( albumIndex > 0 ) {
			albumIndex--;
		}
	}

	/**
	 * Delete the current displayed photo
	 */
	public void deleteCurrentPhoto() {
		if ( albumIndex == -1 ) {
			return;
		}

		if ( albumIndex < album.getPhotos().size() - 1 ) {
			album.getPhotos().remove( albumIndex );
		}
		else {
			album.getPhotos().remove( albumIndex );
			albumIndex--;
		}
	}

	/**
	 * Add a photo the the album being displayed
	 * @param photo - photo to be added
	 * @return true if photo was successfully added to the album; false otherwise
	 */
	public boolean addPhotoToAlbum( Photo photo ) {
		if ( album.addPhoto( photo ) ) {
			albumIndex = album.getPhotos().size() - 1;
			return true;
		}
		else {
			return false;
		}
	}

	/**
	 * Retrieve the current photo being displayed
	 * @return photo object that is currently being displayed
	 */
	public Photo getCurrentPhoto() {
		if ( albumIndex == -1 ) {
			return null;
		}
		else {
			return album.getPhotos().get( albumIndex );
		}
	}
}
