package photos31;

import java.io.*;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.TreeSet;
import classes.Album;
import classes.Photo;
import classes.Tag;
import classes.User;
import javafx.application.Application;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.fxml.FXMLLoader;
import javafx.scene.Node;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.ComboBox;
import javafx.scene.control.ListView;
import javafx.scene.control.TextArea;
import javafx.scene.control.TextField;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.text.Text;
import javafx.stage.FileChooser;
import javafx.stage.FileChooser.ExtensionFilter;
import javafx.stage.Stage;
import states.AdminDashboardState;
import states.AlbumDisplayState;
import states.LoginScreenState;
import states.SearchResultsDisplayState;
import states.State;
import states.UserDashboardState;
/**
 * Main Photos Application
 * 
 * @author Eric Zhang
 * @author Stanley Cai
 *
 */
public class Photos extends Application {
	/**
	 * Current state of the photos application
	 */
	private State state;
	/**
	 * Primary stage of the photos application
	 */
	private Stage primaryStage;

	/**
	 * Start of the photos application
	 * @param primaryStage - primary stage of the photos application
	 */
	@Override
	public void start( Stage primaryStage ) {
		this.primaryStage = primaryStage;
		primaryStage.setTitle( "Photos" );
		primaryStage.setResizable( false );
		try {
			state = new LoginScreenState( this );
			updateRoot();

			primaryStage.setResizable( false );
			primaryStage.setTitle( "Photos" );
			primaryStage.show();
		}
		catch ( Exception e ) {
			System.exit( 0 );
		}
		
		primaryStage.setOnCloseRequest( e -> {
			if ( state instanceof AdminDashboardState ) {
				logoutAdmin();
			}
			else if ( state instanceof UserDashboardState ) {
				logoutUser();
			}
			else if ( state instanceof AlbumDisplayState || state instanceof SearchResultsDisplayState ) {
				returnToUserDashboard();
				logoutUser();
			}
		});
	}

	/**
	 * Update the root node of the stage
	 */
	// Precondition: everything else for the new state is setup
	public void updateRoot() {
		FXMLLoader loader = new FXMLLoader();
		if ( state instanceof LoginScreenState ) {
			loader.setLocation( getClass().getResource( "/fxml/login_screen.fxml" ) );
		}
		else if ( state instanceof AdminDashboardState ) {
			loader.setLocation( getClass().getResource( "/fxml/admin_dashboard.fxml" ) );
		}
		else if ( state instanceof UserDashboardState ) {
			loader.setLocation( getClass().getResource( "/fxml/user_dashboard.fxml" ) );
		}
		else if ( state instanceof AlbumDisplayState ) {
			loader.setLocation( getClass().getResource( "/fxml/album_display.fxml" ) );
		}
		else if ( state instanceof SearchResultsDisplayState ) {
			loader.setLocation( getClass().getResource( "/fxml/search_results_display.fxml" ) );
		}

		try {
			Scene scene = new Scene( loader.load(), 1400, 700 );
			primaryStage.setScene( scene );
		}
		catch ( IOException ex ) {
			return;
		}

		String[] buttonIds = null;

		// Setup signals
		if ( state instanceof LoginScreenState ) {
			buttonIds = new String[] { "loginButton" };
		}
		else if ( state instanceof AdminDashboardState ) {
			buttonIds = new String[] { "createNewUserButton", "deleteUserButton", "saveAndLogoutButton" };

			@SuppressWarnings( "unchecked" )
			ListView<User> userListView = (ListView<User>) lookup( "#userListView" );
			userListView.setItems( ( (AdminDashboardState) state ).getUserListForAdmin() );
		}
		else if ( state instanceof UserDashboardState ) {
			buttonIds = new String[] { "searchPhotosButton", "openAlbumButton", "createNewAlbumButton",
					"renameAlbumButton", "deleteAlbumButton", "saveAndLogoutButton" };

			Text title = (Text) lookup( "#title" );
			title.setText( "Welcome, " + ( (UserDashboardState) state ).getUser() );

			@SuppressWarnings( "unchecked" )
			ListView<Album> albumListView = (ListView<Album>) lookup( "#albumListView" );
			albumListView.setItems( getUser().getAlbums() );

			Button saveAndLogoutButton = lookupButton( "#saveAndLogoutButton" );
			if ( getUser().getUsername().equals( "stock" ) ) {
				saveAndLogoutButton.setText( "Logout (saving unavailable)" );
			}
		}
		else if ( state instanceof AlbumDisplayState ) {
			buttonIds = new String[] { "goToPreviousPhotoButton", "deletePhotoButton", "goToNextPhotoButton",
					"addPhotosButton", "addTagButton", "deleteTagButton", "updateCaptionButton",
					"movePhotoToAlbumButton", "copyPhotoToAlbumButton", "backButton" };

			Text title = (Text) lookup( "#title" );
			title.setText( getAlbum().getName() );

			ImageView photoView = (ImageView) lookup( "#photoView" );
			photoView.setFitWidth( 620 );
			photoView.setFitHeight( 410 );

			@SuppressWarnings( "unchecked" )
			ComboBox<String> tagKeysComboBox = (ComboBox<String>) lookup( "#tagKeysComboBox" );
			tagKeysComboBox.setEditable( true );
			tagKeysComboBox.setItems( getAllTagKeys() );

			TextArea captionInput = (TextArea) lookup( "#captionInput" );
			captionInput.setWrapText( true );

			displayCurrentPhoto();
		}
		else if ( state instanceof SearchResultsDisplayState ) {
			buttonIds = new String[] { "goToPreviousPhotoButton", "goToNextPhotoButton", "createNewAlbumButton",
					"backButton" };

			Text title = (Text) lookup( "#title" );
			title.setText( "Search results for: " + getSearchInput() );

			ImageView photoView = (ImageView) lookup( "#photoView" );
			photoView.setFitWidth( 620 );
			photoView.setFitHeight( 410 );

			displayCurrentPhoto();
		}

		for ( String buttonId : buttonIds ) {
			activateButton( buttonId );
		}
	}

	/**
	 * Look up the button for the specific button provided
	 * @param buttonId - button to be looked up
	 */
	public void activateButton( String buttonId ) {
		Button button = lookupButton( "#" + buttonId );
		activateButton( button );
	}

	/**
	 * Directly provide functionality to the specific button
	 * @param button - button to be invoked
	 */
	public void activateButton( Button button ) {
		button.setOnAction( e -> {
			state.handleSignal( button.getId() );
		} );
	}

	/**
	 * Login via textfield as either a user or admin
	 */
	// Begin LoginScreenState methods
	public void login() {
		TextField input = (TextField) lookup( "#usernameInput" );
		String username = input.getText().strip();
		if ( username.length() > 0 ) {
			if ( username.equalsIgnoreCase( "admin" ) ) {
				loginAdmin();
			}
			else if ( username.equalsIgnoreCase( "stock" ) ) {
				loginStock();
			}
			else {
				loginUser( username );
			}
		}
	}

	/**
	 * Login as an admin
	 */
	@SuppressWarnings( "unchecked" )
	public void loginAdmin() {
		if ( !( state instanceof LoginScreenState ) ) {
			return;
		}

		File usersFolder;
		ObservableList<User> userListForAdmin = null;
		try {
			userListForAdmin = FXCollections.observableArrayList();
			usersFolder = new File( "users" );
			if ( !usersFolder.exists() ) {
				usersFolder.mkdir();
			}

			Text loginError = (Text) lookup( "#loginError" );
			loginError.setText( "" );

		}
		catch ( Exception e ) {
			Text loginError = (Text) lookup( "#loginError" );
			loginError.setText( "Login failed" );
			return;
		}

		for ( File userFile : usersFolder.listFiles() ) {
			String username = getUsernameOfUserFile( userFile );
			if ( username == null || username.equals( "stock" ) ) {
				continue;
			}

			try {
				// Create User object with contents of file as Album
				ObjectInputStream in = new ObjectInputStream(
						new BufferedInputStream( new FileInputStream( userFile ) ) );
				ArrayList<Album> albums = (ArrayList<Album>) in.readObject();
				userListForAdmin.add( new User( username, albums ) );
				in.close();
			}
			catch ( Exception e ) {
			}
		}

		state = new AdminDashboardState( this, userListForAdmin );
		updateRoot();
	}

	/**
	 * Login as a user
	 * @param username - user that is being logged in
	 */
	@SuppressWarnings( "unchecked" )
	public void loginUser( String username ) {
		if ( !( state instanceof LoginScreenState ) ) {
			return;
		}

		User user = null;
		try {
			ObjectInputStream in = new ObjectInputStream(
					new BufferedInputStream( new FileInputStream( new File( "users/" + username + ".user" ) ) ) );
			ArrayList<Album> albums = (ArrayList<Album>) in.readObject();
			user = new User( username, albums );
			in.close();

			Text loginError = (Text) lookup( "#loginError" );
			loginError.setText( "" );
		}
		catch ( FileNotFoundException e ) {
			Text loginError = (Text) lookup( "#loginError" );
			loginError.setText( "User not found" );
			return;
		}
		catch ( Exception e ) {
			Text loginError = (Text) lookup( "#loginError" );
			loginError.setText( "Login failed" );
			return;
		}

		state = new UserDashboardState( this, user );
		updateRoot();
	}

	/**
	 * Login with the special username stock
	 */
	public void loginStock() {
		Album album = new Album( "stock" );
		File stockFolder = new File( "data/stock" );
		for ( File photoFile : stockFolder.listFiles() ) {
			Photo photo = new Photo( photoFile );
			album.addPhoto( photo );
		}

		ArrayList<Album> albums = new ArrayList<>();
		albums.add( album );
		User stock = new User( "stock", albums );

		state = new UserDashboardState( this, stock );
		updateRoot();
	}
	// End LoginScreenState methods

	// Begin AdminDashboardState methods
	/**
	 * Create a new user
	 */
	public void createNewUser() {
		if ( !( state instanceof AdminDashboardState ) ) {
			return;
		}

		TextField usernameInput = (TextField) lookup( "#usernameInput" );
		String username = usernameInput.getText().strip();
		if ( username.equalsIgnoreCase( "admin" ) || username.equalsIgnoreCase( "stock" ) ) {
			return;
		}
		for ( User existingUser : getUserListForAdmin() ) {
			// Do not allow duplicate usernames
			if ( existingUser.getUsername().equalsIgnoreCase( username ) ) {
				return;
			}
		}

		User newUser = new User( username );
		getUserListForAdmin().add( newUser );

		// Clear usernameInput
		usernameInput.setText( "" );
	}

	/**
	 * Delete a user off of the list of users available to admin
	 */
	public void deleteSelectedUser() {
		if ( !( state instanceof AdminDashboardState ) ) {
			return;
		}

		getUserListForAdmin().remove( getSelectedUser() );
	}

	/**
	 * Retrieve the selected user
	 * @return user that is selected
	 */
	public User getSelectedUser() {
		if ( state instanceof AdminDashboardState ) {
			@SuppressWarnings( "unchecked" )
			ListView<User> userListView = (ListView<User>) lookup( "#userListView" );
			return userListView.getSelectionModel().getSelectedItem();
		}
		return null;
	}

	/**
	 * Log out of the admin user
	 */
	public void logoutAdmin() {
		if ( !( state instanceof AdminDashboardState ) ) {
			return;
		}

		File usersFolder = new File( "users" );
		if ( !usersFolder.exists() ) {
			usersFolder.mkdir();
		}

		// Reset usersFolder
		for ( File file : usersFolder.listFiles() ) {
			file.delete();
		}

		for ( User user : getUserListForAdmin() ) {
			File userFile = new File( "users/" + user + ".user" );
			ObjectOutputStream out = null;
			try {
				userFile.createNewFile();
				out = new ObjectOutputStream( new BufferedOutputStream( new FileOutputStream( userFile ) ) );
				out.writeObject( user.getAlbumsArrayList() );
				out.close();
			}
			catch ( IOException e ) {
				if ( out != null ) {
					try {
						out.close();
					}
					catch ( IOException e1 ) {
					}
				}
			}
		}

		state = new LoginScreenState( this );
		updateRoot();
	}

	/**
	 * Retrieve the username of the user's file
	 * @param userFile - specific file of the user
	 * @return username of the user
	 */
	private String getUsernameOfUserFile( File userFile ) {
		int lastIndexOfPeriod = userFile.getName().lastIndexOf( ".user" );
		if ( lastIndexOfPeriod > 0 ) {
			String username = userFile.getName().substring( 0, lastIndexOfPeriod );
			if ( username.equalsIgnoreCase( "admin" ) ) {
				return null;
			}
			return username;
		}
		return null;
	}

	/**
	 * Retrieve the user list available to admin
	 * @return observablelist of users
	 */
	public ObservableList<User> getUserListForAdmin() {
		if ( !( state instanceof AdminDashboardState ) ) {
			return null;
		}

		return ( (AdminDashboardState) state ).getUserListForAdmin();
	}
	// End AdminDashboardState methods

	
	// Begin UserDashboardState methods
	/**
	 * Search for photos by a specific search input
	 */
	public void searchPhotos() {
		if ( !( state instanceof UserDashboardState ) ) {
			return;
		}

		TextField searchPhotosInput = (TextField) lookup( "#searchPhotosInput" );
		String[] data = searchPhotosInput.getText().strip().split( " " );
		try {
			if ( data.length == 1 ) {
				if ( data[0].indexOf( "=" ) >= 0 ) {
					// Single tag
					String[] inputTag = data[0].split( "=" );
					String key = inputTag[0];
					String value = inputTag[1];
					Tag tag = new Tag( key, value );

					ArrayList<Photo> allPhotos = getUser().getAllPhotos();
					ArrayList<Photo> filteredPhotos = new ArrayList<>();

					for ( Photo photo : allPhotos ) {
						if ( photo.lookupTag( tag ) ) {
							filteredPhotos.add( photo );
						}
					}

					state = new SearchResultsDisplayState( this, getUser(), searchPhotosInput.getText().strip(),
							filteredPhotos );
					updateRoot();
				}
				else if ( data[0].indexOf( "-" ) >= 0 ) {
					// Date range
					data = data[0].split( "-" );
					String date1 = data[0];
					String date2 = data[1];

					String[] date1Components = date1.split( "/" );
					int month1 = Integer.parseInt( date1Components[0] );
					int day1 = Integer.parseInt( date1Components[1] );
					int year1 = Integer.parseInt( date1Components[2] );
					Calendar cal1 = Calendar.getInstance();
					int[] constants = new int[] { Calendar.MONTH, Calendar.DAY_OF_MONTH, Calendar.YEAR,
							Calendar.HOUR_OF_DAY, Calendar.MINUTE, Calendar.SECOND, Calendar.MILLISECOND };
					int[] values = new int[] { month1 - 1, day1, year1, 0, 0, 0, 0 };
					for ( int i = 0; i < constants.length; i++ ) {
						cal1.set( constants[i], values[i] );
					}
					long time1 = cal1.getTimeInMillis();

					String[] date2Components = date2.split( "/" );
					int month2 = Integer.parseInt( date2Components[0] );
					int day2 = Integer.parseInt( date2Components[1] );
					int year2 = Integer.parseInt( date2Components[2] );
					Calendar cal2 = Calendar.getInstance();
					values = new int[] { month2 - 1, day2, year2, 23, 59, 59, 999 };
					for ( int i = 0; i < constants.length; i++ ) {
						cal2.set( constants[i], values[i] );
					}
					long time2 = cal2.getTimeInMillis();

					ArrayList<Photo> allPhotos = getUser().getAllPhotos();
					ArrayList<Photo> filteredPhotos = new ArrayList<>();
					for ( Photo photo : allPhotos ) {
						if ( photo.getPhotoTime() >= time1 && photo.getPhotoTime() <= time2 ) {
							filteredPhotos.add( photo );
						}
					}

					state = new SearchResultsDisplayState( this, getUser(), searchPhotosInput.getText().strip(),
							filteredPhotos );
					updateRoot();
				}
			}
			else if ( data.length == 3 ) {
				// Two tags
				String[] inputTag1 = data[0].split( "=" );
				String key1 = inputTag1[0];
				String value1 = inputTag1[1];
				Tag tag1 = new Tag( key1, value1 );

				String[] inputTag2 = data[2].split( "=" );
				String key2 = inputTag2[0];
				String value2 = inputTag2[1];
				Tag tag2 = new Tag( key2, value2 );

				ArrayList<Photo> allPhotos = getUser().getAllPhotos();
				ArrayList<Photo> filteredPhotos = new ArrayList<>();

				if ( data[1].equalsIgnoreCase( "AND" ) ) {
					for ( Photo photo : allPhotos ) {
						if ( photo.lookupTag( tag1 ) && photo.lookupTag( tag2 ) ) {
							filteredPhotos.add( photo );
						}
					}
				}
				else if ( data[1].equalsIgnoreCase( "OR" ) ) {
					for ( Photo photo : allPhotos ) {
						if ( photo.lookupTag( tag1 ) || photo.lookupTag( tag2 ) ) {
							filteredPhotos.add( photo );
						}
					}
				}
				else {
					throw new IllegalArgumentException();
				}

				state = new SearchResultsDisplayState( this, getUser(), searchPhotosInput.getText().strip(),
						filteredPhotos );
				updateRoot();
			}
		}
		catch ( Exception e ) {
		}
	}

	/**
	 * Open the selected album
	 */
	public void openSelectedAlbum() {
		if ( state instanceof UserDashboardState ) {
			User user = ( (UserDashboardState) state ).getUser();
			Album openAlbum = getSelectedAlbum();
			if ( openAlbum != null ) {
				state = new AlbumDisplayState( this, user, openAlbum );
				updateRoot();
			}
		}
	}

	/**
	 * Create a new album
	 */
	public void createNewAlbum() {
		if ( state instanceof UserDashboardState ) {
			TextField albumNameInput = (TextField) lookup( "#albumNameInput1" );
			String albumName = albumNameInput.getText().strip().strip();
			if ( albumName.length() > 0 ) {
				User user = ( (UserDashboardState) state ).getUser();
				if ( user.addAlbum( new Album( albumName ) ) ) {
					// Clear input
					albumNameInput.setText( "" );
				}
			}
		}
	}

	/**
	 * Rename the selected album
	 */
	public void renameSelectedAlbum() {
		if ( !( state instanceof UserDashboardState ) ) {
			return;
		}

		Album selectedAlbum = getSelectedAlbum();
		if ( selectedAlbum != null ) {
			TextField albumNameInput = (TextField) lookup( "#albumNameInput2" );
			String newAlbumName = albumNameInput.getText().strip();

			if ( getUser().canRenameAlbum( selectedAlbum, newAlbumName ) ) {
				// Clear input
				@SuppressWarnings( "unchecked" )
				ListView<Album> albumListView = (ListView<Album>) lookup( "#albumListView" );
				Album renamedAlbum = new Album( newAlbumName, selectedAlbum.getPhotos() );
				albumListView.getItems().set( albumListView.getItems().indexOf( selectedAlbum ), renamedAlbum );
				albumNameInput.setText( "" );
			}
		}
	}

	/**
	 * Delete the selected album
	 */
	public void deleteSelectedAlbum() {
		if ( state instanceof UserDashboardState ) {
			User user = getUser();
			// Since albumListView is connected to album list of user, albumListView will immediately
			// reflect this deletion
			user.deleteAlbum( getSelectedAlbum() );
		}
	}

	/**
	 * Retrieve the selected album
	 * @return album instance that is selected
	 */
	public Album getSelectedAlbum() {
		if ( state instanceof UserDashboardState ) {
			@SuppressWarnings( "unchecked" )
			ListView<Album> albumListView = (ListView<Album>) lookup( "#albumListView" );
			return albumListView.getSelectionModel().getSelectedItem();
		}

		return null;
	}

	/**
	 * Log out of the current user
	 */
	public void logoutUser() {
		if ( state instanceof UserDashboardState ) {
			User user = getUser();
			if ( !user.getUsername().equalsIgnoreCase( "stock" ) ) {
				ObjectOutputStream out = null;
				try {
					out = new ObjectOutputStream( new BufferedOutputStream(
							new FileOutputStream( new File( "users/" + getUser().getUsername() + ".user" ) ) ) );
					out.writeObject( getUser().getAlbumsArrayList() );
					out.close();
				}
				catch ( IOException e ) {
					if ( out != null ) {
						try {
							out.close();
						}
						catch ( IOException e1 ) {
						}
					}
					return;
				}
			}

			state = new LoginScreenState( this );
			updateRoot();
		}

	}
	// End UserDashboardState methods

	/**
	 * Retrieve the current user
	 * @return user instance of the current user
	 */
	public User getUser() {
		if ( state instanceof UserDashboardState ) {
			return ( (UserDashboardState) state ).getUser();
		}
		if ( state instanceof AlbumDisplayState ) {
			return ( (AlbumDisplayState) state ).getUser();
		}
		if ( state instanceof SearchResultsDisplayState ) {
			return ( (SearchResultsDisplayState) state ).getUser();
		}
		return null;
	}

	// Start AlbumDisplayState methods
	/**
	 * Proceed to next photo of the album
	 */
	public void goToNextPhoto() {
		if ( state instanceof AlbumDisplayState ) {
			AlbumDisplayState tempState = (AlbumDisplayState) state;
			if ( tempState.hasNextPhoto() ) {
				tempState.nextPhoto();
				displayCurrentPhoto();
			}
		}
		else if ( state instanceof SearchResultsDisplayState ) {
			SearchResultsDisplayState tempState = (SearchResultsDisplayState) state;
			if ( tempState.hasNextPhoto() ) {
				tempState.nextPhoto();
				displayCurrentPhoto();
			}
		}
	}

	/**
	 * Proceed to previous photo of the album
	 */
	public void goToPreviousPhoto() {
		if ( state instanceof AlbumDisplayState ) {
			AlbumDisplayState tempState = (AlbumDisplayState) state;
			if ( tempState.hasPreviousPhoto() ) {
				tempState.previousPhoto();
				displayCurrentPhoto();
			}
		}
		else if ( state instanceof SearchResultsDisplayState ) {
			SearchResultsDisplayState tempState = (SearchResultsDisplayState) state;
			if ( tempState.hasPreviousPhoto() ) {
				tempState.previousPhoto();
				displayCurrentPhoto();
			}
		}
	}

	/**
	 * Delete the currently displayed photo of the album
	 */
	public void deleteCurrentPhoto() {
		if ( state instanceof AlbumDisplayState ) {
			AlbumDisplayState tempState = (AlbumDisplayState) state;
			tempState.deleteCurrentPhoto();
			displayCurrentPhoto();
		}
	}

	/**
	 * Select specific photos to add to the album
	 */
	public void selectPhotoToAddToAlbum() {
		if ( !( state instanceof AlbumDisplayState ) ) {
			return;
		}

		FileChooser fileChooser = new FileChooser();
		fileChooser.getExtensionFilters()
				.addAll( new ExtensionFilter( "Image Files", "*.bmp", "*.gif", "*.png", "*.jpeg" ) );
		File selectedFile = fileChooser.showOpenDialog( primaryStage );
		if ( selectedFile != null ) {
			Photo photo = new Photo( selectedFile );
			for ( Photo existingPhoto : getUser().getAllPhotos() ) {
				if ( existingPhoto.equals( photo ) ) {
					addPhotoToAlbum( existingPhoto );
				}
			}
			addPhotoToAlbum( photo );
		}
	}

	/**
	 * Add photo instance to the album
	 * @param photo - photo instance to be added to album
	 */
	public void addPhotoToAlbum( Photo photo ) {
		if ( state instanceof AlbumDisplayState ) {
			AlbumDisplayState tempState = (AlbumDisplayState) state;
			tempState.addPhotoToAlbum( photo );
			displayCurrentPhoto();
		}
	}

	/**
	 * Display the current photo the album
	 */
	public void displayCurrentPhoto() {
		if ( state instanceof AlbumDisplayState ) {
			AlbumDisplayState tempState = (AlbumDisplayState) state;
			Photo currentPhoto = tempState.getCurrentPhoto();

			ImageView imageView = (ImageView) lookup( "#photoView" );
			Text photoDate = (Text) lookup( "#photoDate" );
			@SuppressWarnings( "unchecked" )
			ListView<Tag> tagListView = (ListView<Tag>) lookup( "#tagListView" );
			TextArea captionInput = (TextArea) lookup( "#captionInput" );
			try {
				if ( currentPhoto != null ) {
					imageView.setImage( new Image( currentPhoto.getPhotoFile().getAbsolutePath() ) );
					photoDate.setText( "Date of photo: " + currentPhoto.photoDateStringForm() );
					tagListView.setItems( currentPhoto.getTagsObservableList() );
					captionInput.setText( currentPhoto.getCaption() );
				}
				else {
					imageView.setImage( new Image( "resources/empty_album.png" ) );
					photoDate.setText( "Date of photo:" );
					tagListView.setItems( null );
					captionInput.setText( "" );
				}
			}
			catch ( IllegalArgumentException e ) {
				imageView.setImage( new Image( "resources/image_unavailable.png" ) );
				photoDate.setText( "Date of photo:" );
				tagListView.setItems( null );
				captionInput.setText( "" );
			}

			@SuppressWarnings( "unchecked" )
			ComboBox<String> tagKeysComboBox = (ComboBox<String>) lookup( "#tagKeysComboBox" );
			tagKeysComboBox.setValue( null );
			TextField tagValueInput = (TextField) lookup( "#tagValueInput" );
			tagValueInput.setText( "" );
		}
		else if ( state instanceof SearchResultsDisplayState ) {
			SearchResultsDisplayState tempState = (SearchResultsDisplayState) state;
			Photo currentPhoto = tempState.getCurrentPhoto();

			ImageView imageView = (ImageView) lookup( "#photoView" );
			Text photoDate = (Text) lookup( "#photoDate" );
			@SuppressWarnings( "unchecked" )
			ListView<Tag> tagListView = (ListView<Tag>) lookup( "#tagListView" );
			Text caption = (Text) lookup( "#caption" );
			try {
				if ( currentPhoto != null ) {
					imageView.setImage( new Image( currentPhoto.getPhotoFile().getAbsolutePath() ) );
					photoDate.setText( "Date: " + currentPhoto.photoDateStringForm() );
					tagListView.setItems( currentPhoto.getTagsObservableList() );
					caption.setText( currentPhoto.getCaption() );
				}
				else {
					imageView.setImage( new Image( "resources/no_results.png" ) );
					photoDate.setText( "" );
					tagListView.setItems( null );
					caption.setText( "" );
				}
			}
			catch ( IllegalArgumentException e ) {
				imageView.setImage( new Image( "resources/image_unavailable.png" ) );
				photoDate.setText( "" );
				tagListView.setItems( null );
				caption.setText( "" );
			}
		}
	}

	/**
	 * Add a tag to the currently displayed photo of the album
	 */
	public void addTagToPhoto() {
		if ( state instanceof AlbumDisplayState ) {
			Photo currentPhoto = ( (AlbumDisplayState) state ).getCurrentPhoto();
			if ( currentPhoto == null ) {
				return;
			}

			@SuppressWarnings( "unchecked" )
			ComboBox<String> tagKeysComboBox = (ComboBox<String>) lookup( "#tagKeysComboBox" );
			String key = tagKeysComboBox.getSelectionModel().getSelectedItem();
			if ( key == null ) {
				key = tagKeysComboBox.getEditor().getText().strip();
			}
			key = key.replaceAll( " ", "-" );
			TextField tagValueInput = (TextField) lookup( "#tagValueInput" );
			String value = tagValueInput.getText().strip();
			value = value.replaceAll( " ", "-" );

			if ( key == "" || value == "" ) {
				return;
			}

			Tag newTag = new Tag( key.strip(), value.strip() );
			@SuppressWarnings( "unchecked" )
			ListView<Tag> tagListView = (ListView<Tag>) lookup( "#tagListView" );
			for ( Tag existingTag : tagListView.getItems() ) {
				if ( existingTag.equals( newTag ) ) {
					return;
				}
			}
			tagListView.getItems().add( newTag );
			FXCollections.sort( tagListView.getItems() );
			currentPhoto.setTags( tagListView.getItems() );

			tagKeysComboBox.setItems( getAllTagKeys() );
			tagValueInput.setText( "" );
		}
	}

	/**
	 * Delete the selected tag off of the photo
	 */
	public void deleteSelectedTag() {
		if ( !( state instanceof AlbumDisplayState ) ) {
			return;
		}
		Photo currentPhoto = ( (AlbumDisplayState) state ).getCurrentPhoto();
		if ( currentPhoto == null ) {
			return;
		}

		@SuppressWarnings( "unchecked" )
		ListView<Tag> tagListView = (ListView<Tag>) lookup( "#tagListView" );
		Tag selectedTag = tagListView.getSelectionModel().getSelectedItem();
		tagListView.getItems().remove( selectedTag );
		currentPhoto.setTags( tagListView.getItems() );

		@SuppressWarnings( "unchecked" )
		ComboBox<String> tagKeysComboBox = (ComboBox<String>) lookup( "#tagKeysComboBox" );
		tagKeysComboBox.setItems( getAllTagKeys() );
	}

	/**
	 * Update the photo's caption
	 */
	public void updatePhotoCaption() {
		if ( !( state instanceof AlbumDisplayState ) ) {
			return;
		}
		Photo currentPhoto = ( (AlbumDisplayState) state ).getCurrentPhoto();
		if ( currentPhoto == null ) {
			return;
		}

		TextArea captionInput = (TextArea) lookup( "#captionInput" );
		currentPhoto.setCaption( captionInput.getText().strip() );
	}

	/**
	 * Move a photo from one album into another
	 */
	public void movePhoto() {
		if ( !( state instanceof AlbumDisplayState ) ) {
			return;
		}
		Photo currentPhoto = ( (AlbumDisplayState) state ).getCurrentPhoto();
		if ( currentPhoto == null ) {
			return;
		}

		TextField destinationAlbumInput = (TextField) lookup( "#destinationAlbumInput" );
		Album destinationAlbum = getUser().lookupAlbum( destinationAlbumInput.getText().strip() );
		if ( destinationAlbum != null && !destinationAlbum.getName().equals( getAlbum().getName() ) ) {
			destinationAlbum.addPhoto( currentPhoto );
			deleteCurrentPhoto();
		}
	}

	/**
	 * Copy a photo into another album
	 */
	public void copyPhoto() {
		if ( !( state instanceof AlbumDisplayState ) ) {
			return;
		}
		Photo currentPhoto = ( (AlbumDisplayState) state ).getCurrentPhoto();
		if ( currentPhoto == null ) {
			return;
		}

		TextField destinationAlbumInput = (TextField) lookup( "#destinationAlbumInput" );
		Album destinationAlbum = getUser().lookupAlbum( destinationAlbumInput.getText().strip() );
		if ( destinationAlbum != null && !destinationAlbum.getName().equals( getAlbum().getName() ) ) {
			destinationAlbum.addPhoto( currentPhoto );
		}
	}

	/**
	 * Retrieve all the tag keys
	 * @return observablelist of keys
	 */
	public ObservableList<String> getAllTagKeys() {
		if ( state instanceof AlbumDisplayState ) {
			TreeSet<String> temp = new TreeSet<>();
			for ( Album album : getUser().getAlbums() ) {
				for ( Photo photo : album.getPhotos() ) {
					for ( Tag tag : photo.getTagsObservableList() ) {
						temp.add( tag.getKey() );
					}
				}
			}

			ObservableList<String> tagKeys = FXCollections.observableArrayList();
			for ( String key : temp ) {
				tagKeys.add( key );
			}

			return tagKeys;
		}

		return null;
	}

	/**
	 * Navigate back to the user dashboard
	 */
	public void returnToUserDashboard() {
		if ( state instanceof AlbumDisplayState || state instanceof SearchResultsDisplayState ) {
			state = new UserDashboardState( this, getUser() );
			updateRoot();
		}
	}

	/**
	 * Retrieve the current album 
	 * @return instance of the current album
	 */
	public Album getAlbum() {
		if ( !( state instanceof AlbumDisplayState ) ) {
			return null;
		}

		return ( (AlbumDisplayState) state ).getAlbum();
	}

	// End AlbumDisplayState methods

	// Start SearchResultsDisplayState methods

	/**
	 * Retrieve the specific search input inquiry
	 * @return the search input
	 */
	public String getSearchInput() {
		if ( state instanceof SearchResultsDisplayState ) {
			SearchResultsDisplayState tempState = (SearchResultsDisplayState) state;
			return tempState.getSearchInput();
		}
		return null;
	}

	/**
	 * Create a new album with the photos resulting from the specific search input
	 */
	public void createNewAlbumWithSearchResults() {
		if ( state instanceof SearchResultsDisplayState ) {
			SearchResultsDisplayState tempState = (SearchResultsDisplayState) state;
			ArrayList<Photo> photos = tempState.getPhotos();
			TextField newAlbumInput = (TextField) lookup( "#newAlbumInput" );
			String albumName = newAlbumInput.getText().strip();
			Album newAlbum = new Album( albumName, photos );
			getUser().addAlbum( newAlbum );

			// View new album
			state = new AlbumDisplayState( this, getUser(), newAlbum );
			updateRoot();
		}
	}

	// End SearchResultsDisplayState methods

	/**
	 * Retrieve the root of the scene
	 * @return the parent root
	 */
	public Parent getRoot() {
		return primaryStage.getScene().getRoot();
	}

	/**
	 * Look for a node from the root
	 * @param id - id of the node
	 * @return node being searched for
	 */
	public Node lookup( String id ) {
		return getRoot().lookup( id );
	}

	/**
	 * Generate a button to look for a node from the root
	 * @param id - id of the node
	 * @return button with functionality of looking up a node from the root
	 */
	public Button lookupButton( String id ) {
		Node node = getRoot().lookup( id );
		if ( node instanceof Button ) {
			return (Button) node;
		}
		return null;
	}

	/**
	 * Retrieve the current state of the application
	 * @return current state of the application
	 */
	public State getState() {
		return state;
	}

	/**
	 * Set the state of the application
	 * @param state - new state of the application to be set to
	 */
	public void setState( State state ) {
		this.state = state;
	}

	/**
	 * Main method - launching the application
	 */
	public static void main( String[] args ) {
		Application.launch( args );
	}
}
